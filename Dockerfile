FROM node:21-alpine3.18

WORKDIR /app
COPY . .
RUN apk add caddy
RUN wget -qO- https://get.pnpm.io/install.sh | ENV="$HOME/.shrc" SHELL="/bin/sh" sh -
RUN source /root/.shrc && pnpm install
RUN source /root/.shrc && pnpm build

EXPOSE 3000

CMD ["caddy", "run", "--config", "/app/Caddyfile"]
